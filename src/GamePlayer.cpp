/**
 * @file GamePlayer.h
 */

#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <time.h>
#include "GamePlayer.h"

/**
 * constructor that will set up associations with the underlying GPIO objects and initialize instance variables
 * @param buttonPin the integer value of the GPIO pin associated with the player's button input
 * @param greenLedPin the integer value of the GPIO pin associated with the player's green LED output
 * @param redLedPin the integer value of the GPIO pin associated with the player's red LED output
 * @param id the integer identity associated with this player
 */
GamePlayer::GamePlayer(int buttonPin, int greenLedPin, int redLedPin, int id) {
	this->button = new se3910RPi::GPIO(buttonPin, se3910RPi::GPIO::DIRECTION::GPIO_IN, se3910RPi::GPIO::VALUE::GPIO_HIGH);
	this->greenLight = new se3910RPi::GPIO(greenLedPin, se3910RPi::GPIO::DIRECTION::GPIO_OUT, se3910RPi::GPIO::VALUE::GPIO_HIGH);
	this->redLight = new se3910RPi::GPIO(redLedPin, se3910RPi::GPIO::DIRECTION::GPIO_OUT, se3910RPi::GPIO::VALUE::GPIO_HIGH);
	this->id = id;
	this->elapsedTime = 0;
}

/**
 * destructor for the GamePlayer object that will turn any remaining LED's off before deallocating the memory associated with the underlying
 * GPIO objects
 */
GamePlayer::~GamePlayer() {
	// turn lights off before deallocating memory
	this->greenLight->setValue(se3910RPi::GPIO::VALUE::GPIO_HIGH);
	this->redLight->setValue(se3910RPi::GPIO::VALUE::GPIO_HIGH);

	delete this->button;
	delete this->greenLight;
	delete this->redLight;
}

/**
 * contains the logic of the anticipation game. For a total of 10 iterations, a red LED and green LED will be turned on and off. The objective
 * of the player is to press their respective button as soon as possible once the green is turned on after a psuedo-random time interval,
 * in which the red LED will be turned back on, and the green LED off. The player with the lowest total response time is declared the winner.
 */
void GamePlayer::run() {

	for (int i = 0; i < 10; i++) {
		// turn on red light
		this->redLight->setValue(se3910RPi::GPIO::VALUE::GPIO_LOW);

		// generate a value between 0.5 and 5 seconds to sleep before turning green light on
		usleep((std::rand() % 5000 + 500) * 1000);

		// turn off red light and turn on green light
		this->redLight->setValue(se3910RPi::GPIO::VALUE::GPIO_HIGH);
		this->greenLight->setValue(se3910RPi::GPIO::VALUE::GPIO_LOW);

		// get time stamp in which green light was turned on
		timespec lightOn = this->greenLight->getFallingISRTimestamp();

		// wait for button to be pressed
		this->button->enableEdgeInterrupt(se3910RPi::GPIO::GPIO_FALLING);
		this->button->waitForEdge(100000000);

		// get time stamp in which button is pressed
		timespec buttonPress = this->button->getFallingISRTimestamp();

		// turn off green light
		this->greenLight->setValue(se3910RPi::GPIO::VALUE::GPIO_HIGH);

		std::cout << "Player " << this->id << " your latest latency was " << ((buttonPress.tv_nsec - lightOn.tv_nsec) / 100000) << std::endl;

		// add the elapsed time
		this->elapsedTime += buttonPress.tv_nsec - lightOn.tv_nsec;
	}
}

/**
 * returns the total response time of this player object
 */
long GamePlayer::getElapsedTime() {
    return this->elapsedTime;
}

