/**
 * @file main.cpp
 * @author Ryley Powell (powellr)
 * @version 1.0
 *
 * @section LICENSE
 *
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 *
 *
 * This file contains the main function that will start the application, create GamePlayer objects as needed,
 * and spawn threads to run the game for each GamePlayer.
 */

#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <thread>
#include "GamePlayer.h"

/**
 * main function
 * @param player1_gpio_input this is the integer number associated with player 1's button
 * @param player1_gpio_output1 this is the integer number associated with player 1's green LED
 * @param player1_gpio_output2 this is the integer number associated with palyer 1's red LED
 * @param player2_gpio_input this is the integer number associated with player 2's button
 * @param player2_gpio_output1 this is the integer number associated with player 2's green LED
 * @param player2_gpio_output2 this is the integer number associated with palyer 2's red LED
 */
int main(int argc, char* argv[]) {
	if(argc < 7) {
		std::cout << "SE3910 Lab 3 Powell Lux" << std::endl;
		std::cout << "To use: ./anticipation <Player1 GPIO Input> <Player1 GPIO Output 1> <Player1 GPIO Output 2>"
				" <Player 2 GPIO Input> <Player2 GPIO Output 1> <Player2 GPIO Output 2>" << std::endl;
		exit(-1);
	}

	// create players
	// button, green LED, red LED, player ID
	GamePlayer player1(std::atoi(argv[1]), std::atoi(argv[2]), std::atoi(argv[3]), 1);
	GamePlayer player2(std::atoi(argv[4]), std::atoi(argv[5]), std::atoi(argv[6]), 2);

	// create threads to execute player's games
	std::thread p1(&GamePlayer::run, std::ref(player1));
	std::thread p2(&GamePlayer::run, std::ref(player2));

	// synchronize
	p1.join();
	p2.join();


	// announce winner
	if(player1.getElapsedTime() < player2.getElapsedTime()) {
		std::cout << "Player 1 wins with a total elapsed time of " << (player1.getElapsedTime() / 1000000) << " milliseconds!" << std::endl;
	} else if (player2.getElapsedTime() < player1.getElapsedTime()) {
		std::cout << "Player 2 wins with a total elapsed time of " << (player2.getElapsedTime() / 1000000) << " milliseconds!" << std::endl;
	} else {
		std::cout << "It's somehow a draw!" << std::endl;
	}

	// exit
	exit(0);

}
