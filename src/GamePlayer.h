/**
 * @file main.cpp
 * @author Ryley Powell (powellr)
 * @author Lily Lux (luxl)
 * @version 1.0
 *
 * @section LICENSE
 *
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 *
 *
 * This file contains the underlying objects that will be used in order to play the game, as well as the function to play the game
 * that will be called by the main function of the program.
 */
#include "GPIO.h"

#ifndef GAMEPLAYER_H_
#define GAMEPLAYER_H_


class GamePlayer {
private:
    se3910RPi::GPIO *button; /**< This variable will hold the GPIO object tied to the input button >**/
    se3910RPi::GPIO *redLight; /**< This variable will hold the GPIO object tied to the red LED output >**/
    se3910RPi::GPIO *greenLight; /**< This variable will hold the GPIO object tied to the green LED output >**/
    long elapsedTime; /**< This variable will hold the total response time over the course of the game >**/
    int id; /**< This variable will hold the integer identify of the player object >**/
public:
    /**
     * constructor that will set up associations with the underlying GPIO objects and initialize instance variables
     * @param buttonPin the integer value of the GPIO pin associated with the player's button input
     * @param greenLedPin the integer value of the GPIO pin associated with the player's green LED output
     * @param redLedPin the integer value of the GPIO pin associated with the player's red LED output
     * @param id the integer identity associated with this player
     */
	GamePlayer(int buttonPin, int greenLedPin, int redLedPin, int id);

	/**
	 * destructor for the GamePlayer object that will turn any remaining LED's off before deallocating the memory associated with the underlying
	 * GPIO objects
	 */
	virtual ~GamePlayer();

	/**
	 * contains the logic of the anticipation game. For a total of 10 iterations, a red LED and green LED will be turned on and off. The objective
	 * of the player is to press their respective button as soon as possible once the green is turned on after a psuedo-random time interval,
	 * in which the red LED will be turned back on, and the green LED off. The player with the lowest total response time is declared the winner.
	 */
	void run();

	/**
	 * returns the total response time of this player object
	 */
	long getElapsedTime();
};



#endif /* GAMEPLAYER_H_ */
